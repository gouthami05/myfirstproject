package Testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Homepage {
    public WebDriver driver;
    Homepage(WebDriver driver)
    {
        this.driver=driver;
    }
    public void digestauthenitication()
    {
        By authentication=By.cssSelector("a[href*='digest']");
        WebElement loginElement=driver.findElement(authentication);
        loginElement.click();

    }
    public void redirect()
    {
        By redirect=By.cssSelector("a[href='/redirector']");
        WebElement loginElement1=driver.findElement(redirect);
        loginElement1.click();

    }

}
