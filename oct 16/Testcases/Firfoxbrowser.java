package Testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Firfoxbrowser {
    public WebDriver driver;
    String url = "https://the-internet.herokuapp.com";

    @BeforeMethod
    public void set() {
        System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    @Test(enabled = false)
    public void test01() {
        Homepage page = new Homepage(driver);

        page.digestauthenitication();
        driver.get("https://admin:admin@the-internet.herokuapp.com/digest_auth");

        By text=By.cssSelector("div>p");
        WebElement element1=driver.findElement(text);
        //System.out.println(element1.getText());
        String act=element1.getText();
        String expectedresult="Congratulations! You must have the proper credentials.";
        Assert.assertEquals(act,expectedresult);


    }
    @Test
    public void test02()
    {
        Homepage page = new Homepage(driver);
        page.redirect();
        By button=By.cssSelector("#redirect");
        WebElement element2=driver.findElement(button);
        element2.click();
        By text=By.cssSelector("div>h3");
        WebElement element3=driver.findElement(text);
        String actstr=element3.getText();
        String expectstr="Status Codes";
        Assert.assertEquals(actstr,expectstr);


    }
    @AfterMethod
    public void end()
    {
        driver.close();
    }
}
